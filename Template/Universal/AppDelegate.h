//
//  AppDelegate.h
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//
//  INFO: In this file you can edit some of your apps main properties, like API keys
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "RearTableViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "SoundCloudPlayerController.h"
#import "RadioViewController.h"
#import <OneSignal/OneSignal.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#define APP_THEME_COLOR [UIColor colorWithRed:155.0f/255.0f green:49.0f/255.0f blue:230.0f/255.0f alpha:1.0]
#define MENU_BACKGROUND_COLOR_1 [UIColor colorWithRed:48.0f/255.0f green:48.0f/255.0f blue:197.0f/255.0f alpha:1.0]
#define MENU_BACKGROUND_COLOR_2 [UIColor colorWithRed:153.0f/255.0f green:0.0f/255.0f blue:255.0f/255.0f alpha:1.0]

#define NO_CONNECTION_TEXT @"We weren't able to connect to the server. Make sure you have a working internet connection."
#define ABOUT_TEXT @""
#define ABOUT_URL @""

#define ADS_ON false
#define ADMOB_UNIT_ID @""

#define ONESIGNAL_APP_ID @""

#define MAPS_API_KEY @"AIzaSyCxIAWvzTlCrWVaSVQMcP_7-vL2LZy_rqw"

#define YOUTUBE_CONTENT_KEY @"PLACEHOLDER"

#define TWITTER_API @"PLACEHOLDER"
#define TWITTER_API_SECRET @"PLACEHOLDER"
#define TWITTER_TOKEN @"PLACEHOLDER"
#define TWITTER_TOKEN_SECRET @"PLACEHOLDER"

#define FACEBOOK_ACCESS_TOKEN @"EAAMBj96HN5YBAAEWGeZABVxwDzD0NzQ7kSJTG1W8qcvItjsrZCPhZAaDAEPGHC9KjpK8bW5WVZC5ZAIWjQCxTEKZAXGd0DDCICUgjlrWL2lX150Ack1wnxKRIchJlbd2P9ZAgdJZBwxSxc38yBdvdpgTKZAQWUH0rZCz0ZD"
#define INSTAGRAM_ACCESS_TOKEN @"PLACEHOLDER"

#define SOUNDCLOUD_CLIENT @"PLACEHOLDER"

#define OPEN_IN_BROWSER false

@interface AppDelegate : UIResponder <UIApplicationDelegate>  {
    SWRevealViewController *revealController;
    RearTableViewController *rearViewController;
    
    UINavigationController *frontNav;
    UINavigationController *rearNav;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) AVPlayer *player;
@property (nonatomic,strong) NSString *LoginStatusStr;
@property (strong, nonatomic) OneSignal *oneSignal;

//Keeping a reference to controller that is currently playing audio. 
@property (strong, nonatomic) UIViewController* activePlayerController;
- (void) setActivePlayingViewController: (UIViewController *) active;
- (UIViewController *) activePlayingViewController;
- (void) closePlayerWithObserver: (NSObject *) observer;

+ (void) openUrl: (NSString *) url withNavigationController: (UINavigationController *) navController;

@end
