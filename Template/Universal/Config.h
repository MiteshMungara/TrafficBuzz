//
//  Config.h
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject

+ (NSArray *)config;
@end