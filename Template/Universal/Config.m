//
//  Config.m
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//
//  INFO: From this file you can edit your app's main content.
//

#import "Config.h"

@implementation Config {
    
}

+ (NSArray *)config {
    NSMutableArray *sections = [NSMutableArray array];
    
    //-- Please only edit below this line.

    [sections addObject: @[@"TrafficBuzz",
                           @[@"Trafficast", @"WEB", @"http://trafficbuzz.com.my/podcasts/", @"API", @"Trafficast-32"],
                           @[@"Highway", @"WEB", @"http://trafficbuzz.com.my/table", @"API", @"Highway-32"],
                           @[@"Maps", @"WEB", @"https://www.google.com.my/maps/@3.1410294,101.6840722,14z/data=!5m1!1e1", @"API", @"Maps-32"],
                           @[@"Twitter", @"WEB", @"http://trafficbuzz.com.my/social", @"API", @"Twitter-32"],
                           @[@"Facebook", @"FACEBOOK", @"697557763678100", @"API", @"Facebook-32"],
                           ]];
    
    [sections addObject: @[@"Other Sections",
                           @[@"News", @"WEB", @"http://trafficbuzz.com.my/news/", @"API", @"News-32"],
                           @[@"Music", @"WEB", @"https://www.youtube.com/channel/UC-9-kyTW8ZkZNDHQJ6FgpwQ/featured", @"API", @"TV-Show-32"],
                        @[@"Sign Out", @"WEB", @"https://www.youtube.com/channel/UC-9-kyTW8ZkZNDHQJ6FgpwQ/featured",@"API",@""],]];
    
    //@[@"TITLE", @"TYPE", @"CONTENT", @"API"],
    
    //---- Please only edit above this line
    
    return [NSArray arrayWithArray:sections];
}
@end