//
//  FrontNavigationController.m
//  Universal
//
//  Created by Mu-Sonic on 25/10/2015.
//  Copyright © 2016 Sherdle. All rights reserved.
//

#import "SWRevealViewController.h"
#import "FrontNavigationController.h"
#import "AppDelegate.h"
#import "Config.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "RadioViewController.h"
#import "FacebookViewController.h"
#import "MapsViewController.h"
#import "RssViewController.h"
#import "YoutubeViewController.h"
#import "TumblrViewController.h"
#import "WebViewController.h"
#import "WordpressViewController.h"
#import "InstagramViewController.h"
#import "TwitterViewController.h"
#import "SettingViewController.h"
#import "LoginPageViewController.h"
// This affects navbar animation during transitions to transparent bar in detail view.
// White color seems to work best here. APP_THEME_COLOR is another option.
#define NAVBAR_TRANSITION_BGCOLOR [UIColor whiteColor]

@implementation FrontNavigationController
{
    UIColor *prevShadowColor;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    prevShadowColor = self.revealViewController.frontViewShadowColor;
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if (!_selectedIndexPath) {
        _selectedIndexPath  = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    
    NSArray *sectionArray = [Config config];
    NSArray *item = [[sectionArray objectAtIndex: _selectedIndexPath.section] objectAtIndex:(_selectedIndexPath.row + 1)];
    
    [self selectItem:item];
    
    [self configureNavbar];

}

- (void)configureNavbar {
    _gradientView.plainView.backgroundColor = APP_THEME_COLOR;
    
    // attach gradient view just below the nav bar
    [self.view insertSubview:_gradientView belowSubview:self.navigationBar];
    
    // set appearance of status and nav bars
    self.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationBar.shadowImage = [UIImage new];
    [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    //    self.navigationBar.backgroundColor = [UIColor clearColor];
    //    self.navigationBar.barTintColor = [UIColor clearColor];
}


- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    // gradient view to cover both status bar (if present) and nav bar
    CGRect barFrame = self.navigationBar.frame;
    _gradientView.frame = CGRectMake(0, 0, barFrame.size.width, barFrame.origin.y + barFrame.size.height);
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [super pushViewController:viewController animated:animated];
    
    bool hasOneItem = [[Config config] count] == 1 && [[[Config config] objectAtIndex:0] count] == 2;
    
    // add reveal button to the first nav item on the stack
    if (self.viewControllers.count == 1 && !hasOneItem) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 0, 20, 20);
        [btn setImage:[UIImage imageNamed:@"reveal-icon"] forState:UIControlStateNormal];
        [btn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:btn];
        viewController.navigationItem.leftBarButtonItem = leftBarButton;
    }
    
    // switch on navbar transparency
    if (self.viewControllers.count > 1) {
        self.revealViewController.frontViewShadowColor = NAVBAR_TRANSITION_BGCOLOR;
        //[self.gradientView turnTransparencyOn:YES animated:YES];
    }
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    UIViewController *poppedVC = [super popViewControllerAnimated:animated];
    
    // switch off navbar transparency
    if (self.viewControllers.count <= 1) {
        [self.gradientView turnTransparencyOn:NO animated:YES];
        self.revealViewController.frontViewShadowColor = prevShadowColor;
    }
    
    return poppedVC;
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSArray *item = sender;
    NSString *SOCIAL_ITEMS_NAME = [item objectAtIndex: 0];
    NSString *SOCIAL_ITEMS_TYPE = [item objectAtIndex: 1];
    NSString *SOCIAL_ITEMS_URL = [item objectAtIndex: 2];
    NSString *SOCIAL_API = [item objectAtIndex: 3];

    if ([segue.identifier isEqual:@"WORDPRESS"]) {
        WordpressViewController *wordpress = (WordpressViewController *)segue.destinationViewController;
        
        wordpress.firstParam = SOCIAL_ITEMS_URL;
        wordpress.secondParam = [item objectAtIndex: 3];
        wordpress.navTitle = SOCIAL_ITEMS_NAME;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"YOUTUBE"]){
        YoutubeViewController *youTube = (YoutubeViewController *)segue.destinationViewController;
        
        youTube.firstParam = SOCIAL_ITEMS_URL;
        youTube.secondParam = SOCIAL_API;
        youTube.navTitle = SOCIAL_ITEMS_NAME;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"TUMBLR"]) {
        TumblrViewController *tumblr = (TumblrViewController *)segue.destinationViewController;
        
        tumblr.firstParam = SOCIAL_ITEMS_URL;
        tumblr.navTitle = SOCIAL_ITEMS_NAME;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"MAPS"]) {
        MapsViewController *mapView = (MapsViewController *)segue.destinationViewController;
        
        mapView.firstParam = [item objectAtIndex: 2];
        mapView.secondParam = [item objectAtIndex: 3];
        mapView.navTitle = SOCIAL_ITEMS_NAME;
    } else if ([segue.identifier isEqual:@"RADIO"]) {
        RadioViewController *radio = (RadioViewController *)segue.destinationViewController;
        
        radio.firstParam = SOCIAL_ITEMS_URL;
        radio.navTitle = SOCIAL_ITEMS_NAME;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"WEB"]) {
        
        if (![SOCIAL_ITEMS_NAME isEqualToString:@"Sign Out"]) {
            WebViewController *webView = (WebViewController *)segue.destinationViewController;
            
            webView.firstParam = SOCIAL_ITEMS_URL;
            webView.navTitle = SOCIAL_ITEMS_NAME;
        }
       else
       {
           FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
           [loginManager logOut];
           [FBSDKAccessToken setCurrentAccessToken:nil];
           
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"idForLoginKey"];
           
           WebViewController *soundcloud = (WebViewController *)segue.destinationViewController;
           
       }
        
    }  else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"RSS"]) {
        RssViewController *rss = (RssViewController *)segue.destinationViewController;
        
        rss.firstParam = SOCIAL_ITEMS_URL;
        rss.navTitle = SOCIAL_ITEMS_NAME;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"TWITTER"]) {
        TwitterViewController *twitter = (TwitterViewController *)segue.destinationViewController;
        
        twitter.screenName = SOCIAL_ITEMS_URL;
        twitter.navTitle = SOCIAL_ITEMS_NAME;
    }  else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"FACEBOOK"]) {
        FacebookViewController *fb = (FacebookViewController *)segue.destinationViewController;
        
        fb.firstParam = SOCIAL_ITEMS_URL;
        fb.navTitle = SOCIAL_ITEMS_NAME;
    }  else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"INSTAGRAM"]) {
        InstagramViewController *instagram = (InstagramViewController *)segue.destinationViewController;
        
        instagram.firstParam = SOCIAL_ITEMS_URL;
        instagram.navTitle = SOCIAL_ITEMS_NAME;
    }   else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"SOUNDCLOUD"]) {
        SoundCloudViewController *soundcloud = (SoundCloudViewController *)segue.destinationViewController;
    
        soundcloud.firstParam = SOCIAL_ITEMS_URL;
        soundcloud.navTitle = SOCIAL_ITEMS_NAME;
    }   else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"WEB"]) {
//        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
//        [loginManager logOut];
//        [FBSDKAccessToken setCurrentAccessToken:nil];
//        
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"idForLoginKey"];
//        
//        WebViewController *soundcloud = (WebViewController *)segue.destinationViewController;
        
        // soundcloud.firstParam = SOCIAL_ITEMS_URL;
        //soundcloud.navTitle = SOCIAL_ITEMS_NAME;
    }

    //SETTING
}

-(void) selectItem:(NSArray*) item {
    NSString *SOCIAL_ITEMS_TYPE = [item objectAtIndex: 1];
    NSString *idstr=[[NSUserDefaults standardUserDefaults]valueForKey:@"idForLoginKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"idstr :%@",idstr);
    if (idstr.length!=0)
    {}
    [self performSegueWithIdentifier:SOCIAL_ITEMS_TYPE sender:item];
    
}

//- (void)dealloc
//{
//    NSLog(@"Front controller %@ deallocated", _selectedIndexPath);
//}


@end
