//
// WordpressViewController.h
//
// Copyright (c) 2016 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STableViewController.h"
#import "UIImageView+WebCache.h"
#import "MWFeedParser.h"

@interface WordpressViewController : STableViewController <UISearchBarDelegate> {
  
    NSMutableArray *parsedItems;
    
    NSDateFormatter *formatter;
    int page;
    NSDictionary *jsonDict;
    
    UISearchBar *searchBar;
    UIBarButtonItem *searchButton;
    UIBarButtonItem *cancelButton;
    NSString *query;

}

@property(strong,nonatomic)NSString *firstParam;
@property(strong,nonatomic)NSArray *secondParam;
@property(strong,nonatomic)NSString *navTitle;

@end
