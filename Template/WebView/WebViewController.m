//
//  WebViewController.m
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//

#import "WebViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "CommonBanner.h"

#define OFFLINE_FILE_EXTENSION @"html"

@implementation WebViewController
@synthesize firstParam;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (ADS_ON)
        self.canDisplayAds = YES;
    
    self.title= _navTitle;
    
    self.loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    //self.loadingIndicator.hidesWhenStopped = NO;
    [self.loadingIndicator startAnimating];
    self.navigationItem.titleView = self.loadingIndicator;
    
    _webView.delegate = self;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_webView.scrollView addSubview:self.refreshControl]; //<- this is point to use. Add "scrollView" property.
    
    if (self.basicMode){
        self.navigationItem.rightBarButtonItems = nil;
        self.refreshControl.enabled = false;
    }
}


- (void)viewWillAppear:(BOOL)animated {
    NSString *idstr=[[NSUserDefaults standardUserDefaults]valueForKey:@"idForLoginKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"idstr :%@",idstr);
    if (idstr.length==0)
    {
       [self.navigationController setNavigationBarHidden:YES];
        [self performSegueWithIdentifier:@"FBLOGIN" sender:self];
    }
    else
    {
        NSURL *url;
        //Set all the data
        //If the url begins with http (or https for that matter), load it as a webpage. Otherwise, load an asset
        if (self.htmlString) {
            [_webView loadHTMLString:self.htmlString baseURL:[NSURL URLWithString:firstParam]];
        } else {
            //If a string does not start with http, does end with .html and does not contain any slashes, we'll assume it's a local page.
            if (![[firstParam substringToIndex:4] isEqualToString:@"http"] && [firstParam containsString: [NSString stringWithFormat: @".%@", OFFLINE_FILE_EXTENSION]] && ![firstParam containsString: @"/"]){
                firstParam = [firstParam stringByReplacingOccurrencesOfString:
                              [NSString stringWithFormat: @".%@", OFFLINE_FILE_EXTENSION] withString:@""];
                url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:firstParam ofType: OFFLINE_FILE_EXTENSION inDirectory:@"Local"]];
            } else {
                if (![[firstParam substringToIndex:4] isEqualToString:@"http"]){
                    firstParam = [NSString stringWithFormat:@"http://%@", firstParam];
                }
                
                url = [NSURL URLWithString: firstParam];
            }
            
            [_webView loadRequest:[NSURLRequest requestWithURL:url]];
        }

        //HomeV.hidden=NO;
        //[self PickerDown];
        // HomeV.hidden=NO;
    }
}

- (IBAction)goForward:(id)sender {
    [_webView goForward];
}

- (IBAction)goBack:(id)sender {
    [_webView goBack];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    if (![self.refreshControl isRefreshing]){
        self.navigationItem.titleView = self.loadingIndicator;
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    self.navigationItem.titleView = nil;
    
    if (self.refreshControl && [self.refreshControl isRefreshing]){
        [self.refreshControl endRefreshing];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    if (error.code == NSURLErrorNotConnectedToInternet){
        [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NO_CONNECTION_TEXT delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil]show];
    }
    
    self.title = _navTitle;
}

- (void)loadRequest:(NSURLRequest *)request
{
    if ([_webView isLoading])
        [_webView stopLoading];
    [_webView loadRequest:request];
}

- (void)viewWillDisappear
{
    if ([_webView  isLoading])
        [_webView  stopLoading];
}

-(void)handleRefresh:(UIRefreshControl *)refresh {
    // Reload my data
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:_webView.request.URL];
    [_webView loadRequest:requestObj];
}

@end
