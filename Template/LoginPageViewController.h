//
//  LoginPageViewController.h
//  TrafficBuzz
//
//  Created by iSquare2 on 8/12/16.
//  Copyright © 2016 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AppDelegate.h"
@interface LoginPageViewController : UIViewController  <FBSDKLoginButtonDelegate>
{
    AppDelegate *app;
}

-(IBAction)FBLoginBtnPressed:(id)sender;
//@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;
@end
