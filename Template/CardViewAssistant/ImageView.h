//
//  ImageView.h
//  Universal
//
//  Created by Mu-Sonic on 20/11/2015.
//  Copyright © 2016 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageView : UIImageView

- (void)updateAspectRatio;

@end
