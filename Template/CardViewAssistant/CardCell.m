//
//  CardCell.m
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//  Implements Copyright (c) 2014 Audrey Manzano. All rights reserved.
//

#import "CardCell.h"
#import "NSString+HTML.h"
#import "UIViewController+PresentActions.h"
#import "AppDelegate.h"

@implementation CardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       ///
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    _photoView.image = nil;
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

- (IBAction)open:(id)sender {
    [AppDelegate openUrl:_shareUrl withNavigationController:self.parentController.navigationController];
}

- (IBAction)share:(id)sender
{
    NSString *text = _shareUrl;
    NSArray *activityItems = [NSArray arrayWithObjects:text,  nil];
    
    [_parentController presentActions:activityItems sender:sender];
}

- (void)updateImageAspectRatio
{
    [_photoView updateAspectRatio];
    
    // in current implementation the cell will be reloaded anyway
    // [self setNeedsUpdateConstraints];
    // [self setNeedsLayout];
}

+ (CGFloat)heightForRowWithData:(NSDictionary *)data forType:(PageType)type forView:(UIView *)view
{
    NSString *dataCaption, *imageUrl = @"";
    if (type == PageType_INSTAGRAM) {
        if (![[data objectForKey:@"caption"] isKindOfClass:[NSNull class]]) {
            dataCaption = [[data objectForKey:@"caption"] objectForKey:@"text"];
        }
        
        imageUrl = [[[data objectForKey:@"images"] objectForKey:@"low_resolution"] objectForKey:@"url"];
    }
    else if(type == PageType_NEWSFEED) {
        if([data objectForKey:@"message"] != nil)
        {
            dataCaption = [data objectForKey:@"message"];
        }else if([data objectForKey:@"story"] != nil){
            dataCaption = [data objectForKey:@"story"];
        }else if([data objectForKey:@"name"] != nil){
            dataCaption = [data objectForKey:@"name"];
        }else{
            dataCaption = @"";
        }
        if ([data objectForKey:@"picture"] != nil) {
            imageUrl = [data objectForKey:@"picture"];
        }else{
            imageUrl = @"";
        }
    } else {
        if([data objectForKey:@"text"] != nil)
        {
            dataCaption = [data objectForKey:@"text"];
        }else{
            dataCaption = @"";
        }
        if ([data valueForKey:@"extended_entities"] != nil &&
            [[[data valueForKey:@"extended_entities"] valueForKey:@"media"] objectAtIndex:0] != nil &&
            [[[[[data valueForKey:@"extended_entities"] valueForKey:@"media"] objectAtIndex:0] valueForKey:@"type"] isEqualToString: @"photo"]) {
            
            imageUrl = [NSString stringWithFormat:@"%@",[[[[data valueForKey:@"extended_entities"] valueForKey:@"media"] objectAtIndex:0]valueForKey:@"media_url"]];
        } else {
            imageUrl = @"";
        }

    }

    CGSize maximumLabelSize = CGSizeMake(view.frame.size.width - 33, CGFLOAT_MAX);
    
    NSDictionary *attr = @{NSFontAttributeName: [UIFont systemFontOfSize:17]};
    CGRect labelBounds = [[dataCaption stringByDecodingHTMLEntities]  boundingRectWithSize:maximumLabelSize
                                                 options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                              attributes:attr
                                                 context:nil];
 
    //CGSize size = [dataCaption sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(280, CGFLOAT_MAX)];
    CGFloat height =ceilf(labelBounds.size.height) + 450 - 21 ;
    if ([imageUrl isEqualToString:@""]) {
        height -= 290;
    }
    
    return height;
}


@end
