//
//  FooterView.h
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterView : UICollectionReusableView

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
