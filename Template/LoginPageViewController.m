//
//  LoginPageViewController.m
//  TrafficBuzz
//
//  Created by iSquare2 on 8/12/16.
//  Copyright © 2016 Sherdle. All rights reserved.
//

#import "LoginPageViewController.h"
#import "AppDelegate.h"
@interface LoginPageViewController ()

@end

@implementation LoginPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
    /*FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.delegate=self;
    loginButton.delegate = self;
    loginButton.readPermissions = @[@"public_profile", @"email"];
    
    //self.credentialsButton.accessibilityIdentifier = kCredentialsButtonAccessibilityIdentifier;
    loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    loginButton.delegate = self;*/
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
   app.LoginStatusStr = @"0";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Facebook SignIn
-(IBAction)FBLoginBtnPressed:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
   // login.delegate = self;
     [login logInWithReadPermissions: @[@"public_profile", @"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
             //[self getFBProfile];
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             [self getFBProfile];
             NSLog(@"Logged in");
         }
     }];
}

//-(void)profileUpdated:(NSNotification *) notification{

//}
-(void)getFBProfile
{
    NSLog(@"User name: %@",[FBSDKProfile currentProfile].name);
    NSLog(@"User ID: %@",[FBSDKProfile currentProfile].userID);
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 
                 NSLog(@"email user:%@", [result valueForKey:@"email"]);
                 NSLog(@"first_name user:%@", [result valueForKey:@"first_name"]);
                 NSLog(@"last_name user:%@", [result valueForKey:@"last_name"]);
                  NSString *idStr = [NSString stringWithFormat:@"11"];
                 [[NSUserDefaults standardUserDefaults] setObject:idStr forKey:@"idForLoginKey"];
                 app.LoginStatusStr = @"1";
                 [self.navigationController setNavigationBarHidden:NO];
                 [self.navigationController popViewControllerAnimated:YES];
                // [self performSegueWithIdentifier:@"FrontNavigationController" sender:self];
                 
             }
         }];
    }
}


#pragma mark - Relogin Facebook
- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error{
   
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
