//
//  YoutubeDetailViewController.m
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//

#import "YoutubeDetailViewController.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "HCYoutubeParser.h"
#import "FrontNavigationController.h"
#import "UIViewController+PresentActions.h"
#import <MediaPlayer/MediaPlayer.h>

#define LABEL_WIDTH self.articleDetail.tableView.frame.size.width - 20

@implementation YoutubeDetailViewController
{
    ShowPageCell *contentCell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.articleDetail.tableViewDataSource = self;
    self.articleDetail.tableViewDelegate = self;
    
    self.articleDetail.delegate = self;
    self.articleDetail.parallaxScrollFactor = 0.3; // little slower than normal.
    
    self.view.clipsToBounds = YES;
    
    self.articleDetail.headerFade = 100.0f;
    self.articleDetail.defaultimagePagerHeight = 0.0f;

    // after setting the above properties
    [self.articleDetail initialLayout];
    
    [self addImage];
    
    //Make the header/navbar transparent
    FrontNavigationController *nc = (FrontNavigationController *)self.navigationController;
    [nc.gradientView turnTransparencyOn:YES animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"articleDetail"]) {
        self.articleDetail = (DetailViewAssistant *)segue.destinationViewController.view;
        self.articleDetail.parentController = segue.destinationViewController;
    }
}

#pragma mark - UITableView

- (void)addImage {
    UIView *header = self.articleDetail.tableView.tableHeaderView;
    CGRect hRect = header.bounds;
    hRect.size.height = 180;
    header.bounds = hRect;

    [self.articleDetail.playButton addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
    
    [self.articleDetail.youtubeImageView sd_setImageWithURL:[NSURL URLWithString:_imageUrl]
                 placeholderImage:[UIImage imageNamed:@"default_placeholder"]];
    
    self.articleDetail.hasImage = YES;
    
    header.hidden = NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 1) {
        return contentCell ? [contentCell updateWebViewHeightForWidth:tableView.frame.size.width] : 50.0f;
    }
    
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        TitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"titleCell" forIndexPath:indexPath];
        
        cell.lblTitle.text = _titleText;
        cell.lblDescription.text = [NSString stringWithFormat:NSLocalizedString(@"published_on", nil), _date];
        
        return cell;
    }
    else if (indexPath.row == 1) {
        contentCell = (ShowPageCell *)[tableView dequeueReusableCellWithIdentifier:@"ShowPageCell" forIndexPath:indexPath];
        
        contentCell.parentTable = [self.articleDetail getTableView];
        contentCell.parentViewController = self;
        [contentCell loadContent:_summary];
        
        return contentCell;
    }
    else if (indexPath.row == 2) {
        ActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"actionCell" forIndexPath:indexPath];
        cell.actionDelegate = self;
        
        return cell;
    }
    else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reusable"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reusable"];
        }
        
        cell.textLabel.text = @"Default cell";
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor whiteColor];
}

- (void)articleDetail:(DetailViewAssistant *)articleDetail tableViewDidLoad:(UITableView *)tableView
{
//    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat scrollOffset = scrollView.contentOffset.y;
    
    // let the view handle the paralax effect
    [self.articleDetail scrollViewDidScrollWithOffset:scrollOffset];
    
    if (self.articleDetail.hasImage) {
        // switch the nav bar opaque/transparent at the threshold
        FrontNavigationController *nc = (FrontNavigationController *)self.navigationController;
        [nc.gradientView turnTransparencyOn:(scrollOffset < self.articleDetail.headerFade) animated:YES];
    }
}

#pragma mark - UIContentContainer Protocol

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [contentCell updateWebViewHeightForWidth:size.width];
}

#pragma mark - Button actions

- (void)open
{
    
    [AppDelegate openUrl:_videoUrl withNavigationController:self.navigationController];
    
}

- (IBAction)share:(id)sender
{
    NSArray *activityItems = [NSArray arrayWithObjects:_videoUrl,  nil];
    
    [self presentActions:activityItems sender:(id)sender];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playVideo {
    NSDictionary *videos = [HCYoutubeParser h264videosWithYoutubeURL:[NSURL URLWithString:_videoUrl]];
    
    // Presents a MoviePlayerController with the youtube quality medium
    MPMoviePlayerViewController *mp = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:[videos objectForKey:@"medium"]]];
    
    if ([videos count] < 1){
        NSLog(@"Could not retrieve RAW url, opening in Safari instead");
        [self open];
    } else {
        [self presentMoviePlayerViewControllerAnimated:mp];
        //[self presentViewController:mp animated:YES completion:nil];
    }
}

@end
