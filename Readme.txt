------------ Thank you for your purchase! -----------

To get started, open the documentation folder double click the link file. 
You can also browse to http://sherdle.com/support/documentation/

----------- License -------------

For more license information, check the License.txt file inside the template's folder.

------------ Content -----------

- Your template
- Wordpress plugin (fork)
- Documentation

———————————— Upgrading —————————

V2.2
Search for WP & Youtube (and SoundCloud)
Disqus comments for WP
Stability improvements and optimizations

Upgrade Instructions:
Config.m can partially be re-used (Re-setup required for WP items). Image resources, API keys and tokens kan be kept as well. All other files should be updated.


V2.1
SoundCloud!
Notifications over OneSignal!
New Radio layout with metadata support (now playing info) for Shoutcast.
WebView pull-to-refresh
WebView optimizations for loading and navigation buttons
Drawer is hidden when only 1 item is added
Language localization support
Wordpress Imagery improvements
Instagram Token fix
Documentation & code optimizations

Upgrade Instructions:
Config.m and your own image resources can be kept. You can keep your existing tokens and ID's (except for Instagram). All other files and image resources should be updated.


V2.0
Rewritten with StoryBoard & Autolayout
Support for all devices (iPad mini, iPad air, iPad, iPad Pro, iPhone plus)
Support for Split-View
Improved for IOS9
In-App background radio playing
Bug fixes

Upgrade instructions:
Only the config.m, appdelegate.h file and image resources (including assets) can be maintained.


V1.2
Admob Support
Bugfixes
IOS9 (Beta) & XCode 7 (Beta) Support

Upgrade instructions:
Only the config.m file and image resources (including assets) can be maintained.


V1.1
IAd Support

Upgrade instructions:
Only the config.m file and image resources (including assets) can be maintained.


V1.0
Initial release


